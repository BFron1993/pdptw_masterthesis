﻿using PdptwMasterThesis.ObjectModel.AlgoritmLauncher;
using System;

namespace PdptwMasterThesis.ConsoleApp
{
    public static class Program
    {
        static void Main(string[] args)
        {
            string dirPath = @"D:\mAGISTERKA\Large Neighboorhood_instancje_wyniki\pdp_100";
            string outPath = @"C:\Users\barte\Desktop\oUTPUT\LiAndLim100.txt";

            //Launcher launher = new Launcher();
            //launher.RunTests(dirPath, outPath);

            LauncherLiAndLim liAndLimLauncher = new LauncherLiAndLim();
            liAndLimLauncher.RunTests(dirPath, outPath);

            //VNSLauncher vnsLauncher = new VNSLauncher();
            //vnsLauncher.RunTests(dirPath, outPath);

            Console.ReadKey();
        }
    }
}
