﻿using PdptwMasterThesis.ObjectModel.Infrastructure.LiAndLim;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class Solution
    {
        private IList<Truck> _trucks = new List<Truck>();

        public void AddTruck(Truck truck)
        {
            _trucks.Add(truck);
        }

        public int GetNumberOfTrucks()
        {
            return _trucks.Count;
        }
        
        public Truck[] GetTrucks()
        {
            return _trucks.ToArray();
        }

        public List<Commission> RemoveTruck(Truck truck)
        {
            _trucks.Remove(truck);
            return truck.GetCommissinsInside();
        }

        public double GetTotalDistance()
        {
            double sum = 0.0d;

            foreach (Truck truck in _trucks)
            {
                double dist = truck.GetTotalDistace();
                sum += dist;
                //Console.WriteLine(string.Format("Id: {0}, distacne: {1}", truck.Id, dist));
            }

            return sum;
        }

        public Solution GetCopy()
        {
            Solution solution = new Solution();
            foreach (Truck truck in _trucks) solution.AddTruck(truck.GetCopy());
            return solution;
        }

        public void RemoveCommission(Commission commission)
        {
            foreach (Truck truck in _trucks)
            {
                bool removed = truck.RemoveIfContains(commission);
                if (removed) return;
            }
            throw new InvalidOperationException("There is no commission in problem!");
        }

        public BestInsertionForCommission FindBestInsertionForCommission(Commission commision)
        {
            Truck bestTruckToInsert = null;
            double bestCost = double.MaxValue;
            double secodnBestConstToInsert = double.MaxValue;

            foreach (Truck truck in _trucks)
            {
                double? leastIncome = truck.CalculateLeastIncomeForCommission(commision);

                if (leastIncome != null)
                {
                    if (leastIncome < bestCost)
                    {
                        secodnBestConstToInsert = bestCost;
                        bestTruckToInsert = truck;
                        bestCost = leastIncome.Value;
                    }
                    else if (leastIncome < secodnBestConstToInsert)
                    {
                        secodnBestConstToInsert = leastIncome.Value;
                    }
                }
            }

            if (bestTruckToInsert == null) return new BestInsertionForCommission(commision);
            else if (secodnBestConstToInsert == double.MaxValue) return new BestInsertionForCommission(commision, bestTruckToInsert, bestCost);
            else return new BestInsertionForCommission(commision, bestTruckToInsert, bestCost, secodnBestConstToInsert);
        }

        public void RemoveEmptyTrucks()
        {
            IList<Truck> trucks = new List<Truck>();
            foreach (Truck truck in _trucks) if (truck.IsEmpty()) trucks.Add(truck);
            foreach (Truck truck in trucks) _trucks.Remove(truck);
        }

        public bool IsBetterThan(Solution solution)
        {
            if (_trucks.Count != solution._trucks.Count) return _trucks.Count < solution._trucks.Count;
            return GetTotalDistance() < solution.GetTotalDistance();
        }

        public bool IsBetterThanLiAndLim(Solution solution)
        {
            if (_trucks.Count != solution._trucks.Count) return _trucks.Count < solution._trucks.Count;
            if (GetTotalDistance() != solution.GetTotalDistance()) return GetTotalDistance() < solution.GetTotalDistance();
            if (GetTotalWaitingTime() != solution.GetTotalWaitingTime()) return GetTotalWaitingTime() < solution.GetTotalWaitingTime();
            return GetTotalScheduleTime() < solution.GetTotalScheduleTime();
        }

        public List<Commission> RemoveOneTruck()
        {
            Truck truck = _trucks[0];
            _trucks.Remove(truck);

            return truck.GetCommissinsInside();
        }

        public double GetTotalWaitingTime()
        {
            double sum = 0.0d;

            foreach (Truck truck in _trucks)
            {
                sum += truck.GetWaitingTime();
            }

            return sum;
        }

        public double GetTotalScheduleTime()
        {
            double sum = 0.0d;

            foreach (Truck truck in _trucks)
            {
                sum += truck.GetScheduleTime();
            }

            return sum;
        }

        public void PerformDlsWithShiftAndExchange()
        {
            while (true)
	        {
                //Console.WriteLine(GetTotalDistance());
                ExchangeData bestMove = FindBestMove();
                if (bestMove.ExchangeType == ExchangeType.Empty)
                {
                    return;
                }
                else
                {
                    Truck firstTruck = _trucks.Where(x => x.Id == bestMove.FirstGiveTruck).First();
                    Truck secondTruck = _trucks.Where(x => x.Id == bestMove.SecondGiveTruck).First();

                    firstTruck.RemoveCommission(bestMove.FirstCommission);

                    switch (bestMove.ExchangeType)
                    {
                        case ExchangeType.Exchange:
                            secondTruck.RemoveCommission(bestMove.SecondCommission);
                            firstTruck.AddCommissionBestPlace(bestMove.SecondCommission);
                            break;
                    }
                    secondTruck.AddCommissionBestPlace(bestMove.FirstCommission);

                }

                if (bestMove.ExchangeType == ExchangeType.TruckReduced)
                {
                    RemoveEmptyTrucks();
                }
	        }
        }

        private ExchangeData FindBestMove()
        {
            ExchangeData bestMove = new ExchangeData();

            for (int i = 0; i < _trucks.Count; i++)
            {
                for (int j = i + 1; j < _trucks.Count; j++)
                {
                    ExchangeData bestMovebetweenTrucks = FindBestExchangeBetween(_trucks[i], _trucks[j]);
                    if (bestMove.ExchangeType == ExchangeType.Empty)
                    {
                        bestMove = bestMovebetweenTrucks;
                    }
                    if (bestMovebetweenTrucks.ExchangeType == ExchangeType.TruckReduced && bestMove.ExchangeType != ExchangeType.TruckReduced)
                    {
                        bestMove = bestMovebetweenTrucks;
                    }
                    else if (bestMovebetweenTrucks.ExchangeType != ExchangeType.Empty && bestMovebetweenTrucks.TruckParameters.IsBetterThan(bestMove.TruckParameters))
                    {
                        bestMove = bestMovebetweenTrucks;
                    }
                }
            }

            return bestMove;
        }

        private ExchangeData FindBestExchangeBetween(Truck truckOne, Truck truckTwo)
        {
            TruckParameters truckParametersBefore = GetStatesSumUped(truckOne, truckTwo);
            TruckParameters bestMoveRate = new TruckParameters(0.0d, 0.0d, 0.0d);
            ExchangeType exchangeType = ExchangeType.Empty;
            int firstGiveTruckId = 0;
            int secondGiveTruckId = 0;
            Commission firstCommission = null;
            Commission secondCommission = null;


            IList<Commission> commissionsInsideFirstTruck = truckOne.GetCommissinsInside();
            IList<Commission> commissionsInsideSecondTruck = truckTwo.GetCommissinsInside();

            SearchGiveFromFirstTruck(truckOne, truckTwo, truckParametersBefore, ref bestMoveRate, ref exchangeType, ref firstGiveTruckId, ref firstCommission, commissionsInsideFirstTruck, ref secondGiveTruckId);
            SearchGiveFromFirstTruck(truckTwo, truckOne, truckParametersBefore, ref bestMoveRate, ref exchangeType, ref firstGiveTruckId, ref firstCommission, commissionsInsideSecondTruck, ref secondGiveTruckId);

            foreach (Commission commissionInFirstTruck in commissionsInsideFirstTruck)
            {
                PositionInSchedule positionInScheduleOne = truckOne.RemoveCommission(commissionInFirstTruck);

                foreach (Commission commissionInSecondTruck in commissionsInsideSecondTruck)
                {
                    PositionInSchedule positionInScheduleTwo = truckTwo.RemoveCommission(commissionInSecondTruck);

                    bool successOne = truckOne.AddCommissionBestPlace(commissionInSecondTruck);
                    if (successOne)
                    {
                        bool successTwo = truckTwo.AddCommissionBestPlace(commissionInFirstTruck);
                        if (successTwo)
                        {
                            TruckParameters truckParameters = GetStatesSumUped(truckOne, truckTwo);
                            TruckParameters diff = truckParameters.Minus(truckParametersBefore);
                            if (diff.IsBetterThan(bestMoveRate))
                            {
                                bestMoveRate = diff;
                                exchangeType = ExchangeType.Exchange;
                                firstGiveTruckId = truckOne.Id;
                                secondGiveTruckId = truckTwo.Id;
                                firstCommission = commissionInFirstTruck;
                                secondCommission = commissionInSecondTruck;
                            }

                            truckTwo.RemoveCommission(commissionInFirstTruck);
                        }
                        truckOne.RemoveCommission(commissionInSecondTruck);
                    }

                    truckTwo.AddCommission(positionInScheduleTwo, commissionInSecondTruck);
                }

                truckOne.AddCommission(positionInScheduleOne, commissionInFirstTruck);
            }

            switch (exchangeType)
            {
                case ExchangeType.Empty:
                    return new ExchangeData();
                case ExchangeType.Give:
                    return new ExchangeData(firstGiveTruckId, secondGiveTruckId, firstCommission, bestMoveRate, ExchangeType.Give);
                case ExchangeType.TruckReduced:
                    return new ExchangeData(firstGiveTruckId, secondGiveTruckId, firstCommission, bestMoveRate, ExchangeType.TruckReduced);
                case ExchangeType.Exchange:
                    return new ExchangeData(firstGiveTruckId, secondGiveTruckId, firstCommission, secondCommission, bestMoveRate);
                default:
                    throw new InvalidOperationException();
            }
        }

        private static void SearchGiveFromFirstTruck(Truck giveTruck, Truck gaveTruck, TruckParameters truckParametersBefore, ref TruckParameters bestMoveRate, ref ExchangeType exchangeType, ref int firstGiveTruckId, ref Commission firstCommission, IList<Commission> commissionsInsideGiveTruck, ref int secondGavetruckId)
        {
            foreach (Commission commissionToGive in commissionsInsideGiveTruck)
            {
                PositionInSchedule positionInSchedule = giveTruck.RemoveCommission(commissionToGive);
                bool success = gaveTruck.AddCommissionBestPlace(commissionToGive);

                if (success)
                {
                    TruckParameters truckParameters = GetStatesSumUped(giveTruck, gaveTruck);
                    TruckParameters diff = truckParameters.Minus(truckParametersBefore);
                    if (commissionsInsideGiveTruck.Count == 1 && exchangeType != ExchangeType.TruckReduced)
                    {
                        bestMoveRate = diff;
                        exchangeType = ExchangeType.TruckReduced;
                        firstGiveTruckId = giveTruck.Id;
                        secondGavetruckId = gaveTruck.Id;
                        firstCommission = commissionToGive;
                    }
                    else if (diff.IsBetterThan(bestMoveRate))
                    {
                        bestMoveRate = diff;
                        if (commissionsInsideGiveTruck.Count == 1)
                        {
                            exchangeType = ExchangeType.TruckReduced;
                        }
                        else
                        {
                            exchangeType = ExchangeType.Give;
                        }
                        firstGiveTruckId = giveTruck.Id;
                        secondGavetruckId = gaveTruck.Id;
                        firstCommission = commissionToGive;
                    }
                    gaveTruck.RemoveCommission(commissionToGive);
                }
                giveTruck.AddCommission(positionInSchedule, commissionToGive);
            }
        }

        private static TruckParameters GetStatesSumUped(Truck truckOne, Truck truckTwo)
        {
            TruckParameters firstTruckParameters = truckOne.GetTruckParameters();
            TruckParameters secondTruckParameters = truckTwo.GetTruckParameters();
            return firstTruckParameters.Sum(secondTruckParameters);
        }

        public void PerformDlsWithRearange()
        {
            for (int i = 0; i < _trucks.Count; i++)
            {
                PerformDlsRearangeInTruck(_trucks[i]);
            }
            //Console.WriteLine(GetTotalDistance());
        }

        private void PerformDlsRearangeInTruck(Truck truck)
        {
            bool nextRearange = false;
            do
            {
                TruckParameters truckParametersBefore = truck.GetTruckParameters();
                TruckParameters bestMoveRate = new TruckParameters(0.0d, 0.0d, 0.0d);
                Commission bestCommissionToMove = null;
                IList<Commission> commissionsInsideFirstTruck = truck.GetCommissinsInside();
                nextRearange = false;

                foreach (Commission commission in commissionsInsideFirstTruck)
                {
                    PositionInSchedule positionInSchedule = truck.RemoveCommission(commission);
                    truck.AddCommissionBestPlace(commission);

                    TruckParameters truckParameters = truck.GetTruckParameters();
                    TruckParameters diff = truckParameters.Minus(truckParametersBefore);
                    if (diff.IsBetterThan(bestMoveRate))
                    {
                        nextRearange = true;
                        bestMoveRate = diff;
                        bestCommissionToMove = commission;
                    }

                    truck.RemoveCommission(commission);
                    truck.AddCommission(positionInSchedule, commission);
                }

                if (nextRearange)
                {
                    PositionInSchedule positionInSchedule = truck.RemoveCommission(bestCommissionToMove);
                    truck.AddCommissionBestPlace(bestCommissionToMove);
                }
            } while (nextRearange);
        }

        public Truck[] GetTruckNotContainedCommissionInRandom(Commission commission, Random random)
        {
            Truck[] trucksToReturn = _trucks.Where(x => x.Contains(commission) == false).ToArray();
            int trcukCount = trucksToReturn.Count();

            for (int i = 0; i < trcukCount; i++)
            {
                int randIndex = random.Next(trcukCount - 1 - i);
                Truck tmp = trucksToReturn[randIndex];
                trucksToReturn[randIndex] = trucksToReturn[trcukCount - 1 - i];
                trucksToReturn[trcukCount - 1 - i] = tmp;
            }

            return trucksToReturn;
        }

        public Truck GetTruckContainsCommision(Commission commission)
        {
            return _trucks.Where(x => x.Contains(commission)).First();
        }

        public TabooSet GetTabooSet()
        {
            RemoveEmptyTrucks();

            double distance = _trucks.Sum(x => x.GetTotalDistace());
            double waitingTime = _trucks.Sum(x => x.GetWaitingTime());
            double scheduleTime = _trucks.Sum(x => x.GetScheduleTime());
            int numberOftrucks = _trucks.Count();

            return new TabooSet(numberOftrucks, distance, waitingTime, scheduleTime);
        }
    }
}