﻿using System;
namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class Location
    {
        private readonly int _x;
        private readonly int _y;

        public Location(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public double GetDistanceTo(Location location)
        {
            int xDiff = Math.Abs(_x - location._x);
            int yDiff = Math.Abs(_y - location._y);

            return Math.Sqrt((double)((xDiff * xDiff) + (yDiff * yDiff)));
        }
    }
}
