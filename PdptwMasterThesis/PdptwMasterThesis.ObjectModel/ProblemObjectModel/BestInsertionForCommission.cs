﻿namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class BestInsertionForCommission
    {
        public Truck BestInsertionTruck { get; private set; }
        public Commission Commission { get; private set; }

        private double _bestInsertionCost = double.MaxValue;
        private double _secondBestInsertionCost = double.MaxValue;

        public BestInsertionForCommission(Commission commission)
        {
            Commission = commission;
        }

        public BestInsertionForCommission(Commission commission, Truck bestInsertionTruck, double bestInsertionCost) : this(commission)
        {
            BestInsertionTruck = bestInsertionTruck;
            _bestInsertionCost = bestInsertionCost;
        }

        public BestInsertionForCommission(Commission commission, Truck bestInsertionTruck, double bestInsertionCost, double secondBestInsertionCost)
        : this(commission, bestInsertionTruck, bestInsertionCost)
        {
            _secondBestInsertionCost = secondBestInsertionCost;
        }

        public bool HasAnyInsertion()
        {
            return BestInsertionTruck != null;
        }

        public bool IsBetterThan(BestInsertionForCommission bestInsertionForCommission)
        {
            if (BestInsertionTruck == null)
            {
                return true;
            }

            double myRegret = _secondBestInsertionCost - _bestInsertionCost;
            double otherregret = bestInsertionForCommission._secondBestInsertionCost - bestInsertionForCommission._bestInsertionCost;
            return otherregret < myRegret;
        }

        public void Execute()
        {
            BestInsertionTruck.AddCommissionBestPlace(Commission);
        }
    }
}
