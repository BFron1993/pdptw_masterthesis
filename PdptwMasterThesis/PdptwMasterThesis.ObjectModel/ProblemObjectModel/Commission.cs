﻿using System;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class Commission
    {
        private static int _id = 0;

        private double? _distanceToDepot;

        public ActionPlace Pickup { get; private set; }
        public ActionPlace Delivery { get; private set; }
        public int Id { get; private set; }

        public Commission(ActionPlace pickup, ActionPlace delivery)
        {
            Pickup = pickup;
            Delivery = delivery;
            Id = ++_id;
        }

        public double DistanceToDepot
        {
            get
            {
                if (_distanceToDepot == null)
                {
                    throw new InvalidOperationException("Distance to depot have to be calculated first!");
                }
                return _distanceToDepot.Value;
            }
        }

        public void CalulateDistanceToDepot(Location depot)
        {
            double sum = 0.0d;

            sum += depot.GetDistanceTo(Pickup);
            sum += Pickup.GetDistanceTo(Delivery);
            sum += Delivery.GetDistanceTo(depot);

            _distanceToDepot = sum;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            Commission commissionObj = obj as Commission;
            if (commissionObj == null) return false;

            return Id == commissionObj.Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
