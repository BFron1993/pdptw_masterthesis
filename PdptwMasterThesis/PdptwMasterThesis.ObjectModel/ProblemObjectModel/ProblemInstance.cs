﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class ProblemInstance
    {
        private Commission[] _commissions;
        private Random _random;

        public Location Depot { get; private set; }
        public int MaxCapacity { get; private set; }
        public int MinimumTrucks { get; private set; }

        public LinkedList<Commission> Commissions { get { return new LinkedList<Commission>(_commissions); }}

        public ProblemInstance(Location depot, int maxCapacity, IEnumerable<Commission> commissions, Random random, int minimumTrucks)
        {
            Depot = depot;
            MaxCapacity = maxCapacity;
            _commissions = commissions.ToArray();
            _random = random;
            MinimumTrucks = minimumTrucks;
        }

        public LinkedList<Commission> GetRandomCommissionsInRatio(double ratio)
        {
            LinkedList<Commission> ret = new LinkedList<Commission>();
            int commissionCount = _commissions.Count();
            int howMany = Convert.ToInt32(Math.Floor(ratio * commissionCount));

            for (int i = 0; i < howMany; i++)
            {
                int randIndex = _random.Next(commissionCount -1 -i);
                SwapCommission(randIndex, commissionCount - 1 - i);
                ret.AddLast(_commissions[commissionCount - 1 - i]);
            }

            return ret;
        }

        private void SwapCommission(int i, int j)
        {
            Commission commission = _commissions[i];
            _commissions[i] = _commissions[j];
            _commissions[j] = commission;
        }

        public Commission[] GetAllCommissionsInRandomOrder()
        {
            int commissionCount = _commissions.Count();
            Commission[] ret = new Commission[commissionCount];

            for (int i = 0; i < commissionCount; i++)
            {
                int randIndex = _random.Next(commissionCount - 1 - i);
                SwapCommission(randIndex, commissionCount - 1 - i);
                ret[i] = (_commissions[commissionCount - 1 - i]);
            }

            return ret;
        }
    }
}
