﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class PositionInSchedule
    {
        public PositionInSchedule(int? pickupBefore, int deliveryAfter)
        {
            PickupBefore = pickupBefore;
            DeliveryAfter = deliveryAfter;
        }

        public int? PickupBefore { get; private set; }

        public int DeliveryAfter { get; private set; }
    }
}
