﻿using PdptwMasterThesis.ObjectModel.Infrastructure.ActionPlaceMemory;
using System.Collections.Generic;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class ActionPlace : Location
    {
        public int Id { get; private set; }
        public int Demand { get; private set; }
        public int ServiceTime {get; private set; }
        public int TimeWindowStart { get; private set; }
        public int TimeWindowClose { get; private set; }

        private static Dictionary<ActionPlaceMemoryKey, bool> _actionPlacememory = new Dictionary<ActionPlaceMemoryKey, bool>();

        public ActionPlace(int id, int x, int y, int serviceTime, int demand, int timeWindowStart, int timeWindowClose) : base(x, y)
        {
            Id = id;
            Demand = demand;
            ServiceTime = serviceTime;
            TimeWindowStart = timeWindowStart;
            TimeWindowClose = timeWindowClose;
        }

        public override bool Equals(object obj)
        {
            ActionPlace actionPlace = obj as ActionPlace;
            if (actionPlace == null) return false;

            return actionPlace.Id == Id;
        }

        public bool CheckIfCanBeInsertedAfter(ActionPlace beforeActionPlace)
        {
            if (beforeActionPlace == null) return true;

            ActionPlaceMemoryKey actionPlaceMemoryKey = new ActionPlaceMemoryKey(beforeActionPlace.Id, Id);
            bool outValue;
            if(_actionPlacememory.TryGetValue(actionPlaceMemoryKey, out outValue)) return outValue;

            double currentTime = beforeActionPlace.TimeWindowStart;
            currentTime += beforeActionPlace.ServiceTime;
            currentTime += beforeActionPlace.GetDistanceTo(this);

            bool feasible = currentTime <= TimeWindowClose;

            _actionPlacememory.Add(actionPlaceMemoryKey, feasible);
            return feasible;
        }

        public bool CheckIfCanBeInsertedBefore(ActionPlace afterActionPlace)
        {
            if (afterActionPlace == null) return true;

            ActionPlaceMemoryKey actionPlaceMemoryKey = new ActionPlaceMemoryKey(Id, afterActionPlace.Id);
            bool outValue;
            if (_actionPlacememory.TryGetValue(actionPlaceMemoryKey, out outValue)) return outValue;

            double currentTime = TimeWindowStart;
            currentTime += ServiceTime;
            currentTime += GetDistanceTo(afterActionPlace);

            bool feasible = currentTime <= afterActionPlace.TimeWindowClose;
            _actionPlacememory.Add(actionPlaceMemoryKey, feasible);
            return feasible;
        }

        public static void ResetMemory()
        {
            _actionPlacememory.Clear();
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
