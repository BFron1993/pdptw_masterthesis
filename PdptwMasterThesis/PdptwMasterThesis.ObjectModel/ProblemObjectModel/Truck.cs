﻿using PdptwMasterThesis.ObjectModel.Infrastructure.MyList;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class Truck
    {
        private static int _id = 0;

        private HashSet<Commission> _commissionsInside = new HashSet<Commission>();
        private Location _depot;
        private int _maxCapacity;
        private MyList _schedule;
        private double _currentDistance;
        private double _waitingTime;
        private double _scheduleTime;
        private Dictionary<int, double?> _memoryInCaseCalcuateLeastIncome = new Dictionary<int,double?>();

        public int Id { get; private set; }

        public Truck(Location depot, int maxCapacity)
        {
            Initialize(depot, maxCapacity);
            Id = _id++;
        }

        private void Initialize(Location depot, int maxCapacity)
        {
            _schedule = new MyList();
            _depot = depot;
            _maxCapacity = maxCapacity;
            _currentDistance = 0.0d;
            _waitingTime = 0.0d;
            _scheduleTime = 0.0d;
        }

        public Truck(Location depot, int maxCapacity, int id)
        {
            Initialize(depot, maxCapacity);
            Id = id;
        }

        public double GetTotalDistace()
        {
            return _currentDistance;
        }

        private void CalculateCurrentDistanceWaitingTimeAndScheduleTime()
        {
            double totalDist = 0.0d;
            double totalWaitingTime = 0.0d;
            double curentTime = 0.0d;

            Location prevLocation = _depot;
            MyElement pointer = _schedule.Head;
            double dist;

            while (pointer != null)
            {
                dist = prevLocation.GetDistanceTo(pointer.Data);
                curentTime += dist;
                totalDist += dist;
                double waitingTime = pointer.Data.TimeWindowStart - curentTime;
                if (waitingTime > 0)
                {
                    totalWaitingTime += waitingTime;
                    curentTime = pointer.Data.TimeWindowStart;
                }
                curentTime += pointer.Data.ServiceTime;

                prevLocation = pointer.Data;
                pointer = pointer.Next;
            }

            dist = prevLocation.GetDistanceTo(_depot);
            curentTime += dist;
            totalDist += dist;

            _scheduleTime = curentTime;
            _currentDistance = totalDist;
            _waitingTime = totalWaitingTime;
        }

        private void RefreshCurrentDistanceAndMemory()
        {
            CalculateCurrentDistanceWaitingTimeAndScheduleTime();
            _memoryInCaseCalcuateLeastIncome.Clear();
        }

        public double? CalculateLeastIncomeForCommission(Commission commission)
        {
            if(_memoryInCaseCalcuateLeastIncome.ContainsKey(commission.Id))
            {
                return _memoryInCaseCalcuateLeastIncome[commission.Id];
            }

            double newBestDistance = double.MaxValue;

            MyElement nodeToInsertPickupBefore = _schedule.Head;

            while (nodeToInsertPickupBefore != null)
            {
                if (commission.Pickup.CheckIfCanBeInsertedAfter(nodeToInsertPickupBefore.Prev != null ? nodeToInsertPickupBefore.Prev.Data : null)
                    && commission.Pickup.CheckIfCanBeInsertedBefore(nodeToInsertPickupBefore.Data))
                {
                    MyElement nodeToInsertDeliveryAfter = _schedule.AddBeforeElementWithId(nodeToInsertPickupBefore.Data.Id, commission.Pickup);

                    while (nodeToInsertDeliveryAfter != null)
                    {
                        if (commission.Delivery.CheckIfCanBeInsertedAfter(nodeToInsertDeliveryAfter.Data) &&
                            commission.Delivery.CheckIfCanBeInsertedBefore(nodeToInsertDeliveryAfter.Next != null ? nodeToInsertDeliveryAfter.Next.Data : null))
                        {
                            _schedule.AddAfterElemntWithId(nodeToInsertDeliveryAfter.Data.Id, commission.Delivery);

                            if (IsScheaduleFeasible())
                            {
                                CalculateCurrentDistanceWaitingTimeAndScheduleTime();
                                if (_currentDistance < newBestDistance)
                                {
                                    newBestDistance = _currentDistance;
                                }
                            }
                            _schedule.Remove(commission.Delivery.Id);
                        }
                        nodeToInsertDeliveryAfter = nodeToInsertDeliveryAfter.Next;
                    }
                    _schedule.Remove(commission.Pickup.Id);
                }
                nodeToInsertPickupBefore = nodeToInsertPickupBefore.Next;
            }

            if (commission.Pickup.CheckIfCanBeInsertedAfter(_schedule.Tail != null ? _schedule.Tail.Data : null))
            {
                _schedule.AddDataBack(commission.Pickup);
                _schedule.AddDataBack(commission.Delivery);

                if (IsScheaduleFeasible())
                {
                    CalculateCurrentDistanceWaitingTimeAndScheduleTime();
                    if (_currentDistance < newBestDistance)
                    {
                        newBestDistance = _currentDistance;
                    }
                }

                _schedule.Remove(commission.Delivery.Id);
                _schedule.Remove(commission.Pickup.Id);
            }


            CalculateCurrentDistanceWaitingTimeAndScheduleTime();
            if (newBestDistance == double.MaxValue)
            {
                _memoryInCaseCalcuateLeastIncome[commission.Id] = null;
                return null;
            }

            double distToRturn = newBestDistance - _currentDistance;
            _memoryInCaseCalcuateLeastIncome[commission.Id] = distToRturn;
            return distToRturn;
        }

        public void AddCommission(PositionInSchedule positionInShedule, Commission commission)
        {
            if (positionInShedule.PickupBefore != null)
            {
                _schedule.AddBeforeElementWithId(positionInShedule.PickupBefore.Value, commission.Pickup);
            }
            else
            {
                _schedule.AddDataBack(commission.Pickup);
            }

            _schedule.AddAfterElemntWithId(positionInShedule.DeliveryAfter, commission.Delivery);
            RefreshCurrentDistanceAndMemory();
            _commissionsInside.Add(commission);
        }

        public bool AddCommissionBestPlace(Commission commission)
        {
            TruckParameters bestTruckParmeters = new TruckParameters(double.MaxValue, double.MaxValue, double.MaxValue);

            int bestElementIdToInsertPickupBefore = -1;
            int bestElementIdToInsertDeliveryAfter = -1;

            MyElement iteratorForPickup = _schedule.Head;

            while (iteratorForPickup != null)
            {
                if (commission.Pickup.CheckIfCanBeInsertedAfter(iteratorForPickup.Prev != null ? iteratorForPickup.Prev.Data : null)
                    && commission.Pickup.CheckIfCanBeInsertedBefore(iteratorForPickup.Data))
                {
                    MyElement iteratorForDelivery = _schedule.AddBeforeElementWithId(iteratorForPickup.Data.Id, commission.Pickup);

                    while (iteratorForDelivery != null)
                    {
                        if (commission.Delivery.CheckIfCanBeInsertedAfter(iteratorForDelivery.Data) &&
                            commission.Delivery.CheckIfCanBeInsertedBefore(iteratorForDelivery.Next != null ? iteratorForDelivery.Next.Data : null))
                        {
                            MyElement deliveryElement = _schedule.AddAfterElemntWithId(iteratorForDelivery.Data.Id, commission.Delivery);

                            if (IsScheaduleFeasible())
                            {
                                CalculateCurrentDistanceWaitingTimeAndScheduleTime();
                                TruckParameters currentTruckParameters = GetTruckParameters();
                                if (currentTruckParameters.IsBetterThan(bestTruckParmeters))
                                {
                                    bestTruckParmeters = currentTruckParameters;
                                    bestElementIdToInsertPickupBefore = iteratorForPickup.Data.Id;
                                    bestElementIdToInsertDeliveryAfter = iteratorForDelivery.Data.Id;
                                }
                            }

                            _schedule.Remove(commission.Delivery.Id);
                        }
                        iteratorForDelivery = iteratorForDelivery.Next;
                    }
                    _schedule.Remove(commission.Pickup.Id);
                    
                }
                iteratorForPickup = iteratorForPickup.Next;
            }


            if (commission.Pickup.CheckIfCanBeInsertedAfter(_schedule.Tail != null ? _schedule.Tail.Data : null))
            {
                _schedule.AddDataBack(commission.Pickup);
                _schedule.AddDataBack(commission.Delivery);

                if (IsScheaduleFeasible())
                {
                    CalculateCurrentDistanceWaitingTimeAndScheduleTime();
                    TruckParameters currentTruckParameters = GetTruckParameters();
                    if (currentTruckParameters.IsBetterThan(bestTruckParmeters))
                    {
                        RefreshCurrentDistanceAndMemory();
                        _commissionsInside.Add(commission);
                        return true;
                    }
                }

                _schedule.Remove(commission.Delivery.Id);
                _schedule.Remove(commission.Pickup.Id);
            }

            if (bestTruckParmeters.Distance == double.MaxValue)
            {
                return false;
            }

            _schedule.AddBeforeElementWithId(bestElementIdToInsertPickupBefore, commission.Pickup);
            _schedule.AddAfterElemntWithId(bestElementIdToInsertDeliveryAfter, commission.Delivery);
            RefreshCurrentDistanceAndMemory();
            _commissionsInside.Add(commission);
            return true;
        }

        public Truck GetCopy()
        {
            Truck ret = new Truck(_depot, _maxCapacity, Id);
            ret._schedule = _schedule.GetCopy();
            ret.RefreshCurrentDistanceAndMemory();
            ret._commissionsInside = new HashSet<Commission>(_commissionsInside);
            return ret;
        }

        public bool RemoveIfContains(Commission commission)
        {
            if (_commissionsInside.Contains(commission))
            {
                _schedule.Remove(commission.Pickup.Id);
                _schedule.Remove(commission.Delivery.Id);
                RefreshCurrentDistanceAndMemory();
                _commissionsInside.Remove(commission);
                return true;
            }
            return false;
        }

        public bool IsEmpty()
        {
            return _schedule.Head == null;
        }

        private bool IsScheaduleFeasible()
        {
            int currentCapacity = 0;
            double currentTime = 0.0d;
            Location prevLocation = _depot;

            MyElement pointer = _schedule.Head;

            while(pointer != null)
            {
                ActionPlace currentAction = pointer.Data;
                currentTime += prevLocation.GetDistanceTo(currentAction);

                if (currentTime > currentAction.TimeWindowClose)
                {
                    return false;
                }
                else if(currentTime < currentAction.TimeWindowStart)
                {
                    currentTime = currentAction.TimeWindowStart;
                }

                currentCapacity += currentAction.Demand;

                if (currentCapacity > _maxCapacity) return false;

                currentTime += currentAction.ServiceTime;

                prevLocation = currentAction;
                pointer = pointer.Next;
            }

            return true;
        }

        public List<Commission> GetCommissinsInside()
        {
            return _commissionsInside.ToList();
        }

        public double GetWaitingTime() { return _waitingTime; }

        public double GetScheduleTime() { return _scheduleTime; }

        public PositionInSchedule RemoveCommission(Commission commission)
        {
            PositionInSchedule positionInSchedule = _schedule.RemoveCommission(commission);
            RefreshCurrentDistanceAndMemory();
            _commissionsInside.Remove(commission);
            return positionInSchedule;
        }

        public TruckParameters GetTruckParameters()
        {
            return new TruckParameters(_currentDistance, _waitingTime, _scheduleTime);
        }

        public bool Contains(Commission commission)
        {
            return _commissionsInside.Contains(commission);
        }

        public List<PositionInSchedule> GetPossiblePositions(Commission commission)
        {
            List<PositionInSchedule> possiblePositions = new List<PositionInSchedule>();
            
            MyElement nodeToInsertPickupBefore = _schedule.Head;

            while (nodeToInsertPickupBefore != null)
            {
                if (commission.Pickup.CheckIfCanBeInsertedAfter(nodeToInsertPickupBefore.Prev != null ? nodeToInsertPickupBefore.Prev.Data : null)
                    && commission.Pickup.CheckIfCanBeInsertedBefore(nodeToInsertPickupBefore.Data))
                {
                    MyElement nodeToInsertDeliveryAfter = _schedule.AddBeforeElementWithId(nodeToInsertPickupBefore.Data.Id, commission.Pickup);

                    while (nodeToInsertDeliveryAfter != null)
                    {
                        if (commission.Delivery.CheckIfCanBeInsertedAfter(nodeToInsertDeliveryAfter.Data) &&
                            commission.Delivery.CheckIfCanBeInsertedBefore(nodeToInsertDeliveryAfter.Next != null ? nodeToInsertDeliveryAfter.Next.Data : null))
                        {
                            _schedule.AddAfterElemntWithId(nodeToInsertDeliveryAfter.Data.Id, commission.Delivery);

                            if (IsScheaduleFeasible())
                            {
                                possiblePositions.Add(new PositionInSchedule(nodeToInsertPickupBefore.Data.Id, nodeToInsertDeliveryAfter.Data.Id));
                            }
                            _schedule.Remove(commission.Delivery.Id);
                        }
                        nodeToInsertDeliveryAfter = nodeToInsertDeliveryAfter.Next;
                    }
                    _schedule.Remove(commission.Pickup.Id);
                }
                nodeToInsertPickupBefore = nodeToInsertPickupBefore.Next;
            }

            if (commission.Pickup.CheckIfCanBeInsertedAfter(_schedule.Tail != null ? _schedule.Tail.Data : null))
            {
                _schedule.AddDataBack(commission.Pickup);
                _schedule.AddDataBack(commission.Delivery);

                if (IsScheaduleFeasible())
                {
                    possiblePositions.Add(new PositionInSchedule(null, commission.Pickup.Id));
                }

                _schedule.Remove(commission.Delivery.Id);
                _schedule.Remove(commission.Pickup.Id);
            }

            return possiblePositions;
        }
    }
}
