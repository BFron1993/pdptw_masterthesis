﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.ProblemObjectModel
{
    public class TruckParameters
    {
        public TruckParameters(double distance, double waitingTime, double scheduleTime)
        {
            Distance = distance;
            WaitingTime = waitingTime;
            Scheduleime = scheduleTime;
        }

        public double Distance { get; private set; }
        public double WaitingTime { get; private set; }
        public double Scheduleime { get; private set; }

        public bool IsBetterThan(TruckParameters other)
        {
            if (Distance < other.Distance) return true;
            if (Distance > other.Distance) return false;

            if (WaitingTime < other.WaitingTime) return true;
            if (WaitingTime > other.WaitingTime) return false;

            if (Scheduleime < other.Scheduleime) return true;
            return false;
        }

        public TruckParameters Sum(TruckParameters other)
        {
            double distance = Distance + other.Distance;
            double waitingTime = WaitingTime + other.WaitingTime;
            double scheduleTime = Scheduleime + other.Scheduleime;

            return new TruckParameters(distance, waitingTime, scheduleTime);
        }

        public TruckParameters Minus(TruckParameters other)
        {
            double distance = Distance - other.Distance;
            double waitingTime = WaitingTime - other.WaitingTime;
            double scheduleTime = Scheduleime - other.Scheduleime;

            return new TruckParameters(distance, waitingTime, scheduleTime);
        }
    }
}
