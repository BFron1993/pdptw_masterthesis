﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System.Collections.Generic;
using System.Linq;
using System;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.SolomonsInsertion
{
    public class SolomonInsertion
    {
        public Solution GetInitialSolution(ProblemInstance problemInstance)
        {
            ActionPlace.ResetMemory();

            Location depot = problemInstance.Depot;
            LinkedList<Commission> unroutedCommisions = new LinkedList<Commission>(problemInstance.Commissions);
            int maxCapacity = problemInstance.MaxCapacity;

            Solution solution = new Solution();

            while(unroutedCommisions.Count != 0)
            {
                double highestCost = 0.0d;
                Commission hardestCommission = null;

                foreach (Commission commission in unroutedCommisions)
                {
                    double commissionDistance = commission.DistanceToDepot;
                    if (commissionDistance > highestCost)
	                {
		                hardestCommission = commission;
                        highestCost = commissionDistance;
	                }
                }

                Truck truck = new Truck(depot, maxCapacity);
                truck.AddCommissionBestPlace(hardestCommission);
                unroutedCommisions.Remove(hardestCommission);

                do
                {
                    double leastDistanceIncome = double.MaxValue;
                    Commission easiestCommission = null;
 
                    foreach (Commission commission in unroutedCommisions)
	                {
		                double? distanceIncome = truck.CalculateLeastIncomeForCommission(commission);
                        if(distanceIncome != null && leastDistanceIncome > distanceIncome)
                        {
                            easiestCommission = commission;
                            leastDistanceIncome = distanceIncome.Value;
                        }
	                }

                    if (easiestCommission == null)
	                {
                        solution.AddTruck(truck);
		                break;
	                }

                    bool success = truck.AddCommissionBestPlace(easiestCommission);
                    if (!success)
                    {
                        throw new InvalidOperationException("Can not insert commisson into schedule!");
                    }


                    unroutedCommisions.Remove(easiestCommission);
                    if (unroutedCommisions.Count == 0)
                    {
                        solution.AddTruck(truck);
                    }

                }while(unroutedCommisions.Count != 0);
            }

            return solution;
        }
    }
}
