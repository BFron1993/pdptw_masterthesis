﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PdptwMasterThesis.ObjectModel.Infrastructure
{
    public class LargeNeighborhood
    {
        private Random _random;
        private int _wParamater;
        private int _maxFailedIterationsAndTruckDeleted = 0;
        private int _maxFailedIterationsAndNewSolutionFound = 0;

        public LargeNeighborhood(Random random, int wParameter = 30)
        {
            _random = random;
            _wParamater = wParameter;
        }

        public Solution GenerateSolution(Solution initialSolution, ProblemInstance problemInstance)
        {
            double temperature = CalculateinitialTemperature(_wParamater, initialSolution.GetTotalDistance()); ;
	        const double temepratureDecreaseRation = 0.99;
	        const int maxNumberOfIterationsWithoutNewSolution = 250;
	        int iterationsWithoutNewSolution = 0;
	        const double ratioToRemove = 1.0 / 3.0;

            Console.WriteLine(string.Format("Before: {0} {1}", initialSolution.GetNumberOfTrucks(), initialSolution.GetTotalDistance()));

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Solution currentSolution = MinimizeVehicleUsed(problemInstance, initialSolution, ratioToRemove);
            stopWatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}",
            ts.Hours, ts.Minutes, ts.Seconds);
            Console.WriteLine("Reduce trucks part " + elapsedTime);
            Solution bestSolutionEverSeen = currentSolution;

            Console.WriteLine(string.Format("After truck reduction: {0} {1}", bestSolutionEverSeen.GetNumberOfTrucks(), bestSolutionEverSeen.GetTotalDistance()));


            Stopwatch stopWatch2 = new Stopwatch();
            stopWatch2.Start();
            while (iterationsWithoutNewSolution != maxNumberOfIterationsWithoutNewSolution)
	        {
                Solution solutionUnderConstruction = currentSolution.GetCopy();
                LinkedList<Commission> randomCommissionsToRemove = problemInstance.GetRandomCommissionsInRatio(ratioToRemove);

                foreach (Commission commission in randomCommissionsToRemove) solutionUnderConstruction.RemoveCommission(commission);

                bool breakFlag = false;
                while(!breakFlag && randomCommissionsToRemove.Count != 0)
                {
                    BestInsertionForCommission bestInsertion = null;

                    foreach (Commission commission in randomCommissionsToRemove)
                    {
                        BestInsertionForCommission insertionForCommission = solutionUnderConstruction.FindBestInsertionForCommission(commission);

                        if (bestInsertion == null) bestInsertion = insertionForCommission;
                        else if(insertionForCommission.IsBetterThan(bestInsertion)) bestInsertion = insertionForCommission;
                    }

                    if (!bestInsertion.HasAnyInsertion())
                    {
                        breakFlag = true;
                    }
                    else
                    {
                        bestInsertion.Execute();
                        randomCommissionsToRemove.Remove(bestInsertion.Commission);
                    }
                }

                solutionUnderConstruction.RemoveEmptyTrucks();

                if (breakFlag)
                {
                    temperature = temperature * temepratureDecreaseRation;
                    iterationsWithoutNewSolution++;
                    if (iterationsWithoutNewSolution % 25 == 0) Console.WriteLine(iterationsWithoutNewSolution);
                }
                else if(solutionUnderConstruction.IsBetterThan(currentSolution) || accept(currentSolution.GetTotalDistance(), solutionUnderConstruction.GetTotalDistance(), temperature))
                {
                    currentSolution = solutionUnderConstruction;
                    
                    if (currentSolution.IsBetterThan(bestSolutionEverSeen))
                    {
                        if (_maxFailedIterationsAndNewSolutionFound < iterationsWithoutNewSolution) _maxFailedIterationsAndNewSolutionFound = iterationsWithoutNewSolution;

                        iterationsWithoutNewSolution = 0;
                        bestSolutionEverSeen = currentSolution;
                        Console.WriteLine(string.Format("{0} {1}", bestSolutionEverSeen.GetNumberOfTrucks(), bestSolutionEverSeen.GetTotalDistance()));
                    }
                    else
                    {
                        temperature = temperature * temepratureDecreaseRation;
                    }
                }
                else
                {
                    temperature = temperature * temepratureDecreaseRation;
                    iterationsWithoutNewSolution++;
                    if (iterationsWithoutNewSolution % 25 == 0) Console.WriteLine(iterationsWithoutNewSolution);
                }

            }
            stopWatch2.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts2 = stopWatch2.Elapsed;
            string elapsedTime2 = String.Format("{0:00}:{1:00}:{2:00}",
            ts2.Hours, ts2.Minutes, ts2.Seconds);
            Console.WriteLine("Second part " + elapsedTime2);

            return bestSolutionEverSeen;
        }

        private Solution MinimizeVehicleUsed(ProblemInstance problemInstance, Solution initialSolution, double ratioToRemove)
        {
            if (problemInstance.MinimumTrucks == initialSolution.GetNumberOfTrucks()) return initialSolution;

            Solution currentSoution = initialSolution;
            bool foundBetterSolution = false;

            do
            {
                List<Commission> commissionBank = new List<Commission>();
                Solution solutionUnderConstruction = currentSoution.GetCopy();
                int failedIterations = 0;
                commissionBank = commissionBank.Concat(solutionUnderConstruction.RemoveOneTruck()).ToList();
                foundBetterSolution = false;

                do
                {
                    List<Commission> randomCommissions = problemInstance.GetRandomCommissionsInRatio(ratioToRemove).Where(x => !commissionBank.Contains(x)).ToList();
                    foreach (Commission commission in randomCommissions) solutionUnderConstruction.RemoveCommission(commission);
                    commissionBank = commissionBank.Concat(randomCommissions).ToList();

                    bool breakFlag = false;
                    while (!breakFlag && commissionBank.Count != 0)
                    {
                        BestInsertionForCommission bestInsertion = null;

                        foreach (Commission commission in commissionBank)
                        {
                            BestInsertionForCommission insertionForCommission = solutionUnderConstruction.FindBestInsertionForCommission(commission);

                            if (bestInsertion == null) bestInsertion = insertionForCommission;
                            else if (insertionForCommission.IsBetterThan(bestInsertion)) bestInsertion = insertionForCommission;
                        }

                        if (!bestInsertion.HasAnyInsertion())
                        {
                            breakFlag = true;
                        }
                        else
                        {
                            bestInsertion.Execute();
                            commissionBank.Remove(bestInsertion.Commission);
                        }
                    }

                    if (breakFlag)
                    {
                        failedIterations++;
                        if (failedIterations % 250 == 0) Console.WriteLine(failedIterations);
                    }

                } while (failedIterations < 2500 && commissionBank.Count > 0);

                if(commissionBank.Count == 0)
                {
                    if (_maxFailedIterationsAndTruckDeleted < failedIterations)
                    {
                        _maxFailedIterationsAndTruckDeleted = failedIterations;
                        Console.WriteLine("New maxFailedIterationsAndTruckDeleted = {0}", _maxFailedIterationsAndTruckDeleted);
                    }

                    foundBetterSolution = true;
                    currentSoution = solutionUnderConstruction;
                    
                    Console.WriteLine(string.Format("After truck reduction: {0} {1} {2}", currentSoution.GetNumberOfTrucks(), currentSoution.GetTotalDistance(), failedIterations));
                    if (currentSoution.GetNumberOfTrucks() == problemInstance.MinimumTrucks) return currentSoution;
                }

            } while (foundBetterSolution);

            return currentSoution;
        }

        bool accept(double v, double vprim, double temp)
        {
            if (vprim < v)
            {
                return true;
            }
            else if (v == vprim)
            {
                return false;
            }
            else
            {
                double randValue = _random.NextDouble();
                double argument = (vprim - v) / temp;
                double e = Math.Pow(Math.E, -1.0 * argument);
                //Console.WriteLine(string.Format("Accept rate: {0}", e));
                bool accept = randValue < e;
                if (accept)
                {
                    //Console.WriteLine("Accepted!");
                    //Console.WriteLine(string.Format("Temperature: {0}", temp));
                }

                return accept;
            }
        }

        private double CalculateinitialTemperature(int wParameter, double initialSolutionDistnace)
        {
            double wInPercent = wParameter / 100.0d;
            double differenceToBeAcceptIn50Percent = wInPercent * initialSolutionDistnace;

            return differenceToBeAcceptIn50Percent / 0.693147d;
        }

        public int getMaxFailedIterationsAndTruckDeleted()
        {
            return _maxFailedIterationsAndTruckDeleted;
        }

        public int getMaxFailedIterationsAndNewSolutionFound()
        {
            return _maxFailedIterationsAndNewSolutionFound;
        }
    }
}
