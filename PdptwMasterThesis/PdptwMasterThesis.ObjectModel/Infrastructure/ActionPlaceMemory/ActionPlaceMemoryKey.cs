﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.ActionPlaceMemory
{
    public class ActionPlaceMemoryKey
    {
        private int _actionPlaceMemoryBeforeId;
        private int _actionPlaceAfter;

        public ActionPlaceMemoryKey(int before, int after)
        {
            _actionPlaceMemoryBeforeId = before;
            _actionPlaceAfter = after;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            ActionPlaceMemoryKey objAs = obj as ActionPlaceMemoryKey;
            if (objAs == null) return false;
            return _actionPlaceMemoryBeforeId == objAs._actionPlaceMemoryBeforeId && _actionPlaceAfter == objAs._actionPlaceAfter;
        }

        public override int GetHashCode()
        {
                int  hashcode = 23;
                hashcode = (hashcode * 37) + _actionPlaceMemoryBeforeId;
                hashcode = (hashcode * 37) + _actionPlaceAfter;
                return hashcode;
        }

    }
}
