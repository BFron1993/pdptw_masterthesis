﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.LiAndLim
{
    public enum ExchangeType
    {
        Empty = 0,
        Give = 1,
        Exchange = 2,
        TruckReduced = 3
    }
}
