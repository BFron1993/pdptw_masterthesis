﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.LiAndLim
{
    public class ExchangeData
    {
        private int _firstGiveTruckId;
        private int _secondGiveTruckId;
        private Commission _firstCommission;
        private Commission _secondCommission;
        private TruckParameters _truckpaameters;

        public ExchangeType ExchangeType { get; private set; }

        public int FirstGiveTruck
        {
            get
            {
                if (ExchangeType != ExchangeType.Empty)
                {
                    return _firstGiveTruckId;
                }
                throw new InvalidOperationException();
            }
        }

        public int SecondGiveTruck
        {
            get
            {
                if (ExchangeType != ExchangeType.Empty)
                {
                    return _secondGiveTruckId;
                }
                throw new InvalidOperationException();
            }
        }

        public Commission FirstCommission
        {
            get
            {
                if (ExchangeType != ExchangeType.Empty)
                {
                    return _firstCommission;
                }
                throw new InvalidOperationException();
            }
        }

        public Commission SecondCommission
        {
            get
            {
                if (ExchangeType == ExchangeType.Exchange)
                {
                    return _secondCommission;
                }
                throw new InvalidOperationException();
            }
        }

        public TruckParameters TruckParameters
        {
            get
            {
                if (ExchangeType != ExchangeType.Empty)
                {
                    return _truckpaameters;
                }
                throw new InvalidOperationException();
            }
        }

        public ExchangeData()
        {
            ExchangeType = Infrastructure.LiAndLim.ExchangeType.Empty;
        }

        public ExchangeData(int truckGive, int truckGiven, Commission giveCommissiom, TruckParameters truckParameters, ExchangeType exchangeType)
        {
            ExchangeType = exchangeType;
            _firstGiveTruckId = truckGive;
            _secondGiveTruckId = truckGiven;
            _firstCommission = giveCommissiom;
            _truckpaameters = truckParameters;
        }

        public ExchangeData(int firstTruckId, int secondTruckId, Commission firstCommission, Commission secondCommission,  TruckParameters truckParameters)
        {
            ExchangeType = Infrastructure.LiAndLim.ExchangeType.Exchange;
            _firstGiveTruckId = firstTruckId;
            _secondGiveTruckId = secondTruckId;
            _firstCommission = firstCommission;
            _secondCommission = secondCommission;
            _truckpaameters = truckParameters;
        }
    }
}
