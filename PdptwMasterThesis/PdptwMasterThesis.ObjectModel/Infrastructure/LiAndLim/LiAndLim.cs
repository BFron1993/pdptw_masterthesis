﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.LiAndLim
{
    public class LiAndLim
    {
        private int _maxNoImprovement = 100;
        private int _maxAnnealingWithoutImrovement = 10;
        private double _temp;
        private int _wParameter;
        private const double _temepratureDecreaseRation = 0.99;
        private const double _temperatureIncreaseRatio = 1.001;
        private HashSet<TabooSet> _tabooSet = new HashSet<TabooSet>();
        private ProblemInstance _problemInstance;
        private Random _random;

        public LiAndLim(Random random, int wParameter = 100)
        {
            _wParameter = wParameter;
            _random = random;
        }

        private void IncreaseTemperature()
        {
            _temp = _temp * _temperatureIncreaseRatio;
        }

        public Solution GenerateSolution(Solution initialSolution, ProblemInstance problemInstance)
        {
            Solution bestSolution = initialSolution;
            int noImprovement = 0;
            _problemInstance = problemInstance;


            _temp = CalculateinitialTemperature(bestSolution.GetTabooSet().GetCost());
            

            while (noImprovement < _maxNoImprovement)
            {
                bool stopAlgo;
                Solution currentSolution = TabuEmbeded(bestSolution.GetCopy(), out stopAlgo, problemInstance.MinimumTrucks);
                if (stopAlgo)
                {
                    return bestSolution;
                }

                if (currentSolution.IsBetterThanLiAndLim(bestSolution))
                {
                    noImprovement = 0;
                    bestSolution = currentSolution.GetCopy();
                    Console.WriteLine(bestSolution.GetTotalDistance().ToString() + "\t" + bestSolution.GetNumberOfTrucks().ToString());
                    Console.WriteLine("Temp: " + _temp);
                }
                else
                {
                    ++noImprovement;
                    if (noImprovement % 10 == 0)
                    {
                        Console.WriteLine(noImprovement);
                        Console.WriteLine("Temp: " + _temp);
                    }

                }
            }

            return bestSolution;
        }

        public Solution TabuEmbeded(Solution solution, out bool stopAlgo, int minTrucks)
        {
            solution.PerformDlsWithShiftAndExchange();
            solution.PerformDlsWithRearange();
            int noImprovement = 0;
            Solution bestSolution = solution.GetCopy();

            Solution currentSolution = solution.GetCopy();
            
            while (noImprovement < _maxAnnealingWithoutImrovement)
            {
                bool stop = MetropolisProc(currentSolution);
                if (stop)
                {
                    stopAlgo = true;
                    return bestSolution;
                }

                if (minTrucks < currentSolution.GetNumberOfTrucks())
                {
                    currentSolution = TryToRemoveTrucks(currentSolution);
                }
                
                currentSolution.PerformDlsWithShiftAndExchange();
                currentSolution.PerformDlsWithRearange();

                if (currentSolution.IsBetterThanLiAndLim(bestSolution))
                {
                    bestSolution = currentSolution.GetCopy();
                    noImprovement = 0;
                    Console.WriteLine(bestSolution.GetTotalDistance().ToString() + "\t" + bestSolution.GetNumberOfTrucks().ToString());
                }
                else
                {
                    noImprovement++;
                }
            }

            stopAlgo = false;
            return bestSolution;
        }

        private double CalculateinitialTemperature(double initialSolutionDistnace)
        {
            double wInPercent = _wParameter / 100.0d;
            double differenceToBeAcceptIn50Percent = wInPercent * initialSolutionDistnace;

            return differenceToBeAcceptIn50Percent / 0.693147d;
        }

        private void DecreaseTemperature()
        {
            _temp = _temp * _temepratureDecreaseRation;
            //Console.WriteLine("Temp: " + _temp.ToString());
        }

        //private void IncreaseTemperature()
        //{
        //    _temp = _temp + 1.0d;
        //}

        private bool MetropolisProc(Solution solution)
        {
            TabooSet oldTabooSet;
            double oldCost;
            bool accepted = false;

            do
            {
                int maxMoveAllowedPerStep = 10;
                int currentSteps = 0;

                oldTabooSet = solution.GetTabooSet();
                oldCost = oldTabooSet.GetCost();
                TabooSet tabooSet = MakeMove(solution);
                currentSteps++;

                while(tabooSet == null && currentSteps < maxMoveAllowedPerStep)
                {
                    MakeMove(solution, false);
                    oldTabooSet = solution.GetTabooSet();
                    oldCost = oldTabooSet.GetCost();
                    tabooSet = MakeMove(solution);
                    currentSteps++;
                }

                if (tabooSet == null)
                {
                    return true;
                }

                double cost = tabooSet.GetCost();
                double delta = cost - oldCost;
                if (delta <= 0.0 || tabooSet.NumberOfTrucks() < oldTabooSet.NumberOfTrucks())
                {
                    accepted = true;
                    _tabooSet.Add(tabooSet);
                }
                else
                {
                    double randValue = _random.NextDouble();
                    double argument = (delta) / _temp;
                    double e = Math.Pow(Math.E, -1.0 * argument);
                    //Console.WriteLine(string.Format("Accept rate: {0}", e));
                    bool accept = randValue < e;
                    if (accept)
                    {
                        accepted = true;
                        _tabooSet.Add(tabooSet);
                    }
                    else
                    {
                        IncreaseTemperature();
                    }
                }
            } while (accepted == false);

            DecreaseTemperature();
            return false;
        }

        private TabooSet MakeMove(Solution solution, bool notIntaboo = true)
        {
            Commission[] commissions = _problemInstance.GetAllCommissionsInRandomOrder();
            int randomInt = _random.Next(2);

            if (randomInt == 0) //Move first
            {
                TabooSet tabooSet = TryToMove(commissions, solution, notIntaboo);
                if (tabooSet == null)
                {
                    tabooSet = TryToExchange(commissions, solution, notIntaboo);
                    if (tabooSet == null)
                    {
                        return null;
                    }
                }
                return tabooSet;
            }
            else // Exchange first
            {
                TabooSet tabooSet = TryToExchange(commissions, solution, notIntaboo);
                if (tabooSet == null)
                {
                    tabooSet = TryToMove(commissions, solution, notIntaboo);
                    if (tabooSet == null)
                    {
                        return null;
                    }
                }
                return tabooSet;
            }
        }

        private TabooSet TryToExchange(Commission[] commissions, Solution solution, bool notIntaboo)
        {
            for (int i = 0; i < commissions.Length - 1; i++)
            {
                for (int j = i + 1; j < commissions.Length; j++)
                {
                    Commission commission1 = commissions[i];
                    Commission commission2 = commissions[j];
                    Truck truck1 = solution.GetTruckContainsCommision(commission1);
                    Truck truck2 = solution.GetTruckContainsCommision(commission2);

                    if (truck1.Id != truck2.Id)
                    {
                        PositionInSchedule position1 = truck1.RemoveCommission(commission1);
                        PositionInSchedule position2 = truck2.RemoveCommission(commission2);

                        List<PositionInSchedule> possiblePositionsIntruck2 = truck2.GetPossiblePositions(commission1);
                        if (possiblePositionsIntruck2.Count > 0)
                        {
                            Shuffle(possiblePositionsIntruck2, _random);
                            foreach (PositionInSchedule positionTruck2 in possiblePositionsIntruck2)
                            {
                                truck2.AddCommission(positionTruck2, commission1);
                                List<PositionInSchedule> possiblePositionsIntruck1 = truck1.GetPossiblePositions(commission2);
                                if (possiblePositionsIntruck1.Count > 0)
                                {
                                    Shuffle(possiblePositionsIntruck1, _random);
                                    foreach (PositionInSchedule positionTruck1 in possiblePositionsIntruck1)
                                    {
                                        truck1.AddCommission(positionTruck1, commission2);
                                        TabooSet tabooSet = solution.GetTabooSet();
                                        if (notIntaboo == false || _tabooSet.Contains(tabooSet) == false)
                                        {
                                            return tabooSet;
                                        }
                                        else
                                        {
                                            truck1.RemoveCommission(commission2);
                                        }
                                    }
                                }
                                truck2.RemoveCommission(commission1);
                            }
                        }
                        truck1.AddCommission(position1, commission1);
                        truck2.AddCommission(position2, commission2);
                    }
                }
            }
            return null;
        }

        private TabooSet TryToMove(Commission[] commissions, Solution solution, bool notIntaboo)
        {
            for (int i = 0; i < commissions.Length; i++)
            {
                Commission commission = commissions[i];
                Truck[] otherTrucks = solution.GetTruckNotContainedCommissionInRandom(commission, _random);
                Truck truck = solution.GetTruckContainsCommision(commission);

                for (int j = 0; j < otherTrucks.Length; j++)
                {
                    Truck destination = otherTrucks[j];
                    List<PositionInSchedule> possiblePositions = destination.GetPossiblePositions(commission);
                    if (possiblePositions.Count > 0)
                    {
                        Shuffle(possiblePositions, _random);

                        foreach (PositionInSchedule item in possiblePositions)
                        {
                            destination.AddCommission(item, commission);
		                    PositionInSchedule position = truck.RemoveCommission(commission);

                            TabooSet tabooSet = solution.GetTabooSet();
                            if (notIntaboo == false || _tabooSet.Contains(tabooSet) == false)
                            {
                                return tabooSet;
                            }
                            truck.AddCommission(position, commission);
                            destination.RemoveCommission(commission);
	                    }
                    }
                }
            }
            return null;
        }

        public static void Shuffle(IList<PositionInSchedule> list, Random rng)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                PositionInSchedule value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static Solution TryToRemoveTrucks(Solution solution)
        {
            Solution workingCopy = solution.GetCopy();
            int numberOfTrucks = solution.GetNumberOfTrucks();
            for (int i = 0; i < numberOfTrucks; i++)
            {
                Truck[] trucks = workingCopy.GetTrucks();
                Truck toRemove = trucks[i];
                IList<Commission> commissions = workingCopy.RemoveTruck(toRemove);

                bool breakFlag = false;
                while (!breakFlag && commissions.Count != 0)
                {
                    BestInsertionForCommission bestInsertion = null;

                    foreach (Commission commission in commissions)
                    {
                        BestInsertionForCommission insertionForCommission = workingCopy.FindBestInsertionForCommission(commission);

                        if (bestInsertion == null) bestInsertion = insertionForCommission;
                        else if (insertionForCommission.IsBetterThan(bestInsertion)) bestInsertion = insertionForCommission;
                    }

                    if (!bestInsertion.HasAnyInsertion())
                    {
                        breakFlag = true;
                    }
                    else
                    {
                        bestInsertion.Execute();
                        commissions.Remove(bestInsertion.Commission);
                    }
                }

                if (!breakFlag)
                {
                    Console.WriteLine("Truk reduced: " + workingCopy.GetNumberOfTrucks());
                    return workingCopy;
                }

                workingCopy = solution.GetCopy();
            }

            return solution;
        }
    }
}
