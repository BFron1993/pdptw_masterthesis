﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.LiAndLim
{
    public class TabooSet
    {
        private int _numberOfVehicles;
        private double _distance;
        private double _waitingTime;
        private double _scheduleTime;

        public TabooSet(int numberOfVehicles, double distance, double waitingTime, double scheduleTime)
        {
            _numberOfVehicles = numberOfVehicles;
            _distance = distance;
            _waitingTime = waitingTime;
            _scheduleTime = scheduleTime;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }

            TabooSet other = (TabooSet)obj;
            if (_numberOfVehicles != other._numberOfVehicles) return false;
            if (_distance != other._distance) return false;
            if (_waitingTime != other._waitingTime) return false;
            if (_scheduleTime != other._scheduleTime) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return _numberOfVehicles + _distance.GetHashCode() * 2 + _waitingTime.GetHashCode() * 4 + _scheduleTime.GetHashCode() * 8;
        }

        public double GetCost()
        {
            return _distance + _distance * 0.01d * _waitingTime;
        }

        public int NumberOfTrucks()
        {
            return _numberOfVehicles;
        }
    }
}
