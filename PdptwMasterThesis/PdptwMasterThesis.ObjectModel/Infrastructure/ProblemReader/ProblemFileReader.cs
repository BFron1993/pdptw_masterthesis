﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System.Collections.Generic;
using System.IO;
using System;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.ProblemReader
{
    public class ProblemFileReader
    {
        public ProblemInstance ReadFromFile(string path, Random random)
        {
            using(StreamReader file =   new StreamReader(path))
            {
                string currentLine = null;
                string[] tokens = null;

                ReadNextLine(file, ref currentLine, ref tokens);
                int maxCapacity = int.Parse(tokens[1]);
                int minimumTrucks = int.Parse(tokens[3]);

                ReadNextLine(file, ref currentLine, ref tokens);
                int x = int.Parse(tokens[1]);
                int y = int.Parse(tokens[2]);
                Location depot = new Location(x, y);

                IDictionary<int, ActionPlace> pickups = new Dictionary<int, ActionPlace>();
                IDictionary<int, ActionPlace> deliveries = new Dictionary<int, ActionPlace>();

                ReadNextLine(file, ref currentLine, ref tokens);
                while(currentLine != null)
                {
                    int id = int.Parse(tokens[0]);
                    x = int.Parse(tokens[1]);
                    y = int.Parse(tokens[2]);
                    int demand = int.Parse(tokens[3]);
                    int windowStart = int.Parse(tokens[4]);
                    int windowClose = int.Parse(tokens[5]);
                    int serviceTime = int.Parse(tokens[6]);
                    int pickup = int.Parse(tokens[7]);
                    int delivery = int.Parse(tokens[8]);

                    ActionPlace action = new ActionPlace(id, x, y, serviceTime, demand, windowStart, windowClose);
                    if (pickup != 0)
			        {
				        deliveries[id] = action;
			        }
			        else
			        {
                        pickups[delivery] = action;
			        }
                    ReadNextLine(file, ref currentLine, ref tokens);
                }

                List<Commission> commissions = new List<Commission>();

                foreach(int deliveryPairId in pickups.Keys)
                {
                    ActionPlace pickupActionPlace = pickups[deliveryPairId];
                    ActionPlace deliveryActionPlace = deliveries[deliveryPairId];
                    Commission commission = new Commission(pickupActionPlace, deliveryActionPlace);
                    commissions.Add(commission);
                }

                commissions.ForEach(item => item.CalulateDistanceToDepot(depot));

                return new ProblemInstance(depot, maxCapacity, commissions, random, minimumTrucks);
            }
        }

        private static void ReadNextLine(StreamReader file, ref string currentLine, ref string[] tokens)
        {
            currentLine = file.ReadLine();
            tokens = currentLine != null ? currentLine.Split('\t') : null;            
        }
    }
}
