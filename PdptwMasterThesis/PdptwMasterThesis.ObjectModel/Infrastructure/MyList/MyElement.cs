﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.MyList
{
    public class MyElement
    {
        public ActionPlace Data { get; private set; }
        public MyElement Next { get; private set; }
        public MyElement Prev { get; private set; }

        public void SetNext(MyElement element) { Next = element; }
        public void SetPrev(MyElement element) { Prev = element; }

        public MyElement(ActionPlace data, MyElement prev, MyElement next)
        {
            if (data == null) throw new InvalidOperationException("Can not initialize with null!");
            Data = data;
            Next = next;
            Prev = prev;
        }
    }
}
