﻿using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;
using System.Collections.Generic;
namespace PdptwMasterThesis.ObjectModel.Infrastructure.MyList
{
    public class MyList
    {
        private Dictionary<int, MyElement> _elementsInside = new Dictionary<int, MyElement>();

        public MyElement Head { get; private set; }
        public MyElement Tail { get; private set; } 

        public MyList()
        {
            Head = null;
            Tail = null;
        }

        public void Remove(int id)
        {
            CheckIfElementWithIdExists(id);
            MyElement toRomve = _elementsInside[id];

            Remove(toRomve);
        }

        private void Remove(MyElement toRomve)
        {
            MyElement removedPrev = toRomve.Prev;
            MyElement removedNext = toRomve.Next;

            if (removedPrev == null)
            {
                Head = removedNext;
            }
            else
            {
                removedPrev.SetNext(removedNext);
            }

            if (removedNext == null)
            {
                Tail = removedPrev;
            }
            else
            {
                removedNext.SetPrev(removedPrev);
            }

            _elementsInside.Remove(toRomve.Data.Id);
        }



        public MyElement AddDataFront(ActionPlace data)
        {
            CheckIfElementWithIdAlreadyAdded(data.Id);
            MyElement newElement;

            if (Head == null)
	        {
                newElement = new MyElement(data, null, null);
                Tail = newElement;
	        }
            else
            {
                newElement = new MyElement(data, null, Head);
                Head.SetPrev(newElement);
            }

            Head = newElement;
            _elementsInside[data.Id] = newElement;
            return newElement;
        }



        public MyElement AddDataBack(ActionPlace data)
        {
            CheckIfElementWithIdAlreadyAdded(data.Id);
            MyElement newElement;

            if (Tail == null)
            {
                newElement = new MyElement(data, null, null);
                Head = newElement;
            }
            else
            {
                newElement = new MyElement(data, Tail, null);
                Tail.SetNext(newElement);
            }

            Tail = newElement;
            _elementsInside[data.Id] = newElement;
            return newElement;
        }

        public MyElement AddAfterElemntWithId(int id, ActionPlace data)
        {
            CheckIfElementWithIdExists(id);
            CheckIfElementWithIdAlreadyAdded(data.Id);

            MyElement afterElement = _elementsInside[id];
            MyElement newElement;

            if (afterElement.Next == null)
            {
                newElement = new MyElement(data, afterElement, null);
                Tail = newElement;
            }
            else
            {
                MyElement nextElement = afterElement.Next;
                newElement = new MyElement(data, afterElement, nextElement);
                nextElement.SetPrev(newElement);
            }

            afterElement.SetNext(newElement);
            _elementsInside[data.Id] = newElement;
            return newElement;
        }

        public MyElement AddBeforeElementWithId(int id, ActionPlace data)
        {
            CheckIfElementWithIdExists(id);
            CheckIfElementWithIdAlreadyAdded(data.Id);

            MyElement beforeElement = _elementsInside[id];
            MyElement newElement;

            if (beforeElement.Prev == null)
            {
                newElement = new MyElement(data, null, beforeElement);
                Head = newElement;
            }
            else
            {
                MyElement prevElement = beforeElement.Prev;
                newElement = new MyElement(data, prevElement, beforeElement);
                prevElement.SetNext(newElement);
            }

            beforeElement.SetPrev(newElement);
            _elementsInside[data.Id] = newElement;
            return newElement;
        }

        public MyList GetCopy()
        {
            MyList copy = new MyList();

            MyElement pointer = Head;

            while(pointer != null)
            {
                copy.AddDataBack(pointer.Data);
                pointer = pointer.Next;
            }

            return copy;
        }

        public bool ContainsElementWithId(int id)
        {
            return _elementsInside.ContainsKey(id);
        }

        private void CheckIfElementWithIdExists(int id)
        {
            if (!_elementsInside.ContainsKey(id)) throw new InvalidOperationException(string.Format("Action place with id={0} can not be found in collection!", id));
        }

        private void CheckIfElementWithIdAlreadyAdded(int id)
        {
            if (_elementsInside.ContainsKey(id)) throw new InvalidOperationException(string.Format("Action place with id={0} already added!", id));
        }

        public PositionInSchedule RemoveCommission(Commission commission)
        {
            MyElement pickup = _elementsInside[commission.Pickup.Id];
            MyElement delivery = _elementsInside[commission.Delivery.Id];

            int deliveryAfterElemetWithId = delivery.Prev.Data.Id;
            Remove(delivery);

            int? pickupBeforeElementWithId = pickup.Next == null ? (int?) null : pickup.Next.Data.Id;
            Remove(pickup);

            return new PositionInSchedule(pickupBeforeElementWithId, deliveryAfterElemetWithId);
        }
    }
}
