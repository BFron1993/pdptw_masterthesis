﻿using PdptwMasterThesis.ObjectModel.Infrastructure.LiAndLim;
using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.VNS
{
    public class Vns
    {
        private Random _random;
        private ProblemInstance _problemInstance;

        public Vns(Random random)
        {
            _random = random;
        }

        public Solution GenerateSolution(Solution initialSolution, ProblemInstance problemInstance)
        {
            _problemInstance = problemInstance;
            int maxK = 10;//Convert.ToInt32(Math.Ceiling(Math.Sqrt(problemInstance.Commissions.Count)));
            int numBerOfiterationWithoutImprovement = maxK * 100;
            Console.WriteLine(numBerOfiterationWithoutImprovement);
            int numberOfIteationWithoutImprovement = 0;
            initialSolution.PerformDlsWithShiftAndExchange();
            initialSolution.PerformDlsWithRearange();
            Solution bestSolution = initialSolution.GetCopy();
            Console.WriteLine(bestSolution.GetTotalDistance().ToString() + "\t" + bestSolution.GetNumberOfTrucks().ToString());

            while (numberOfIteationWithoutImprovement < numBerOfiterationWithoutImprovement)
            {
                int currentK = 1;
                bool foundBetter = false;
                while (currentK <= maxK && !foundBetter)
                {
                    Solution generatedSolution = PerformVns(bestSolution.GetCopy(), currentK);
                    if (generatedSolution == null)
                    {
                        return bestSolution;
                    }



                    if (generatedSolution.IsBetterThanLiAndLim(bestSolution))
                    {
                        bestSolution = generatedSolution.GetCopy();
                        foundBetter = true;
                        numberOfIteationWithoutImprovement = 0;
                        Console.WriteLine(bestSolution.GetTotalDistance().ToString() + "\t" + bestSolution.GetNumberOfTrucks().ToString());
                    }
                    else
                    {
                        currentK++;
                        numberOfIteationWithoutImprovement++;
                        if (numberOfIteationWithoutImprovement % 10 == 0)
                        {
                            Console.WriteLine(numberOfIteationWithoutImprovement);
                        }

                    }
                }
                
            }

            return bestSolution;
        }

        private Solution PerformVns(Solution solution, int currentK)
        {
            int howManySteps = CountNumberOfSteps.CalculatenumberOfSteps(currentK, _random);

            for (int i = 0; i < howManySteps; i++)
            {
                if (!MakeMove(solution))
                {
                    return null;
                }
            }

            solution.PerformDlsWithShiftAndExchange();
            solution.PerformDlsWithRearange();

            return solution;
        }

        private bool MakeMove(Solution solution)
        {
            Commission[] commissions = _problemInstance.GetAllCommissionsInRandomOrder();
            int randomInt = _random.Next(2);

            if (randomInt == 0) //Move first
            {
                bool tabooSet = TryToMove(commissions, solution);
                if (tabooSet == false)
                {
                    tabooSet = TryToExchange(commissions, solution);
                    if (tabooSet == false)
                    {
                        return false;
                    }
                }
                return true;
            }
            else // Exchange first
            {
                bool tabooSet = TryToExchange(commissions, solution);
                if (tabooSet == false)
                {
                    tabooSet = TryToMove(commissions, solution);
                    if (tabooSet == false)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private bool TryToExchange(Commission[] commissions, Solution solution)
        {
            for (int i = 0; i < commissions.Length - 1; i++)
            {
                for (int j = i + 1; j < commissions.Length; j++)
                {
                    Commission commission1 = commissions[i];
                    Commission commission2 = commissions[j];
                    Truck truck1 = solution.GetTruckContainsCommision(commission1);
                    Truck truck2 = solution.GetTruckContainsCommision(commission2);

                    if (truck1.Id != truck2.Id)
                    {
                        PositionInSchedule position1 = truck1.RemoveCommission(commission1);
                        PositionInSchedule position2 = truck2.RemoveCommission(commission2);

                        List<PositionInSchedule> possiblePositionsIntruck2 = truck2.GetPossiblePositions(commission1);
                        if (possiblePositionsIntruck2.Count > 0)
                        {
                            Shuffle(possiblePositionsIntruck2, _random);
                            foreach (PositionInSchedule positionTruck2 in possiblePositionsIntruck2)
                            {
                                truck2.AddCommission(positionTruck2, commission1);
                                List<PositionInSchedule> possiblePositionsIntruck1 = truck1.GetPossiblePositions(commission2);
                                if (possiblePositionsIntruck1.Count > 0)
                                {
                                    Shuffle(possiblePositionsIntruck1, _random);
                                    foreach (PositionInSchedule positionTruck1 in possiblePositionsIntruck1)
                                    {
                                        truck1.AddCommission(positionTruck1, commission2);

                                        return true;

                                    }
                                }
                                truck2.RemoveCommission(commission1);
                            }
                        }
                        truck1.AddCommission(position1, commission1);
                        truck2.AddCommission(position2, commission2);
                    }
                }
            }
            return false;
        }

        private bool TryToMove(Commission[] commissions, Solution solution)
        {
            for (int i = 0; i < commissions.Length; i++)
            {
                Commission commission = commissions[i];
                Truck[] otherTrucks = solution.GetTruckNotContainedCommissionInRandom(commission, _random);
                Truck truck = solution.GetTruckContainsCommision(commission);

                for (int j = 0; j < otherTrucks.Length; j++)
                {
                    Truck destination = otherTrucks[j];
                    List<PositionInSchedule> possiblePositions = destination.GetPossiblePositions(commission);
                    if (possiblePositions.Count > 0)
                    {
                        Shuffle(possiblePositions, _random);

                        foreach (PositionInSchedule item in possiblePositions)
                        {
                            destination.AddCommission(item, commission);
                            PositionInSchedule position = truck.RemoveCommission(commission);

                             return true;
                        }
                    }
                }
            }
            return false;
        }

        public static void Shuffle(IList<PositionInSchedule> list, Random rng)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                PositionInSchedule value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
