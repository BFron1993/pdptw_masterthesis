﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.Infrastructure.VNS
{
    public  static class CountNumberOfSteps
    {
        public static int CalculatenumberOfSteps(int maxK, Random random)
        {
            if (maxK == 1) return 1;

            double randValue = random.NextDouble() * maxK * maxK;


            for (int i = 1; i <= maxK; i++)
            {
                if (randValue < i * i)
                {
                    return i;
                }   
            }

            throw new Exception("Something gone wrong!");
        }
    }
}
