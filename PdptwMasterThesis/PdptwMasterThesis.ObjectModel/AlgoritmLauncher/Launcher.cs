﻿using PdptwMasterThesis.ObjectModel.Infrastructure;
using PdptwMasterThesis.ObjectModel.Infrastructure.ProblemReader;
using PdptwMasterThesis.ObjectModel.Infrastructure.SolomonsInsertion;
using PdptwMasterThesis.ObjectModel.ProblemObjectModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdptwMasterThesis.ObjectModel.AlgoritmLauncher
{
    public class Launcher
    {
        // pdp_100 - 56
        // pdp_200 - 60
        // pdp_400 - 60
        // pdp-600 - 60
        // pdp_800 - 60
        // pdp_1000 - 58
        // all - 354

        public void RunTests(string problemDiretory, string outPath)
        {
            DirectoryInfo directory = new DirectoryInfo(problemDiretory);
            ProblemFileReader problemFileReader = new ProblemFileReader();
            SolomonInsertion solominInsertion = new SolomonInsertion();
            FileInfo outPathFileInfo = new FileInfo(outPath);
            Random random = new Random();
            LargeNeighborhood largeNeighborhood = new LargeNeighborhood(random);

            using (StreamWriter a = outPathFileInfo.AppendText())
            {
                IEnumerable<FileInfo> files = directory.EnumerateFiles("*", SearchOption.AllDirectories);
                int counter = 0;
                int allCount = 176;


                foreach (FileInfo file in files)
                {
                    Console.WriteLine(file.Name);
                    string filepath = file.FullName;
                    ProblemInstance problemInstance = problemFileReader.ReadFromFile(filepath, random);
                    Solution solution = solominInsertion.GetInitialSolution(problemInstance);
                    Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();
                    solution = largeNeighborhood.GenerateSolution(solution, problemInstance);
                    stopWatch.Stop();
                    // Get the elapsed time as a TimeSpan value.
                    TimeSpan ts = stopWatch.Elapsed;
                    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}",
                    ts.Hours, ts.Minutes, ts.Seconds);
                    Console.WriteLine("Whole algorithm " + elapsedTime);

                    string summary = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", file.FullName, file.Name, solution.GetNumberOfTrucks(), solution.GetTotalDistance(), elapsedTime);

                    a.WriteLine(summary);
                    a.Flush();

                    Console.WriteLine(summary);
                    Console.WriteLine(string.Format("{0:0.00} %", ((double)(++counter * 100)/(double)allCount)));
                }
            }

            Console.WriteLine(largeNeighborhood.getMaxFailedIterationsAndTruckDeleted());
            Console.WriteLine(largeNeighborhood.getMaxFailedIterationsAndNewSolutionFound());
        }
    }
  }
